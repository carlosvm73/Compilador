import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Validation } from '../component/validation';

@Component({
  selector: 'lexicografico-page',
  templateUrl: 'lexicografico.html',
})
export class LexicograficoPage {
  items={};
  keypress={};
  constructor(public navCtrl: NavController) {
  }

  sintax(data){
    this.keypress = new Validation().TableOperations(data);
  }

  execute(data) {
    this.items = new Validation().TableOperations(data);
  }


}
