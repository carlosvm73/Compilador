export class Separatedoflines{
  private replaceAll(text, search, replace){
    while(text.toString().indexOf(search) !=-1){
      text = text.toString().replace(search, replace);
    }
    return text;
  }
  private divideLines(string){
    if(string !== undefined){
      return this.replaceAll(string, String.fromCharCode(59),String.fromCharCode(10)).split(String.fromCharCode(10)).filter(value => value.length !== 0);
    }
  }
  public getStringsLines(string){
    if(string !== undefined){
      let tmp=[];
      for(let lines of this.divideLines(string)){
        tmp.push(lines.trim().split(' ').filter(value => value.length !== 0));
      }
      return tmp;
    }
  }
}
