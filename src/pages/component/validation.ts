import { Separatedoflines } from './separatedoflines';
import { Table } from './table'
export class Validation {
  separator = new Separatedoflines();
  table = new Table();
  private identify(caracter) {
    let char = caracter.charCodeAt();
    if (char >= 48 && char <= 57) return { state: true, type: 'Number', core: caracter };
    if (char >= 97 && char <= 122) return { state: true, type: 'Letter', core: caracter };
    if (char >= 126 && char <= 255 || char === 124 || char === 95 || char === 96 || char === 64 || char === 63 || char >= 32 && char <= 36) return { state: false, type: 'Error', core: caracter };
    if (char >= 37 && char <= 47 || char >= 58 && char <= 62) return { state: true, type: 'Symbol', core: caracter };
    return { state: false, type: 'Error', core: caracter };
  }
  public isValid(token) {
    if (token !== undefined) {
      let char = token.split('');
      let getTypeIndentify: any = this.identify(char[0]);
      if (getTypeIndentify.type === 'Letter') {
        let count = 0;
        for (let item of char) {
          let tmp = this.identify(item).type;
          if (tmp === "Number" || tmp === "Letter") {
            count++;
          } else {
            return { state: false, type: 'Error', message: `Error type ${tmp}`, core: token };
          }
        }
        if (count === char.length) {
          return { state: true, type: 'Success', message: `null`, core: token };
        }
      } else { return { state: false, type: 'Error', message: `Error type ${getTypeIndentify.type}`, core: token }; }
    } else { return { state: false, type: 'Error', message: `Error type unknow`, core: token }; }
  }
  public TableOperations(data) {
    let arrayError = [];
    let arrayReserved = [];
    let arrayDefinedByUser = [];
    let newData = this.separator.getStringsLines(data);

    newData.forEach((valueColumn, keyColumn) => {
      valueColumn.forEach((valueRow, keyRow) => {
        let compare = this.compareTo(valueRow);
        if (compare.status === 'Reserved') {
          arrayReserved.push({core:compare.core, line:keyColumn, message:compare.message, typeToken:this.typeToken(compare.core)});
        } else if (compare.status === 'User') {
          arrayDefinedByUser.push({core:compare.core, line:keyColumn, message:compare.message, typeToken:this.typeToken(compare.core)});
        } else if (compare.status === 'Error'){
          arrayError.push({core:compare.core, line:keyColumn, message:compare.message, typeToken:this.typeToken(compare.core)});
        }


      })
    })
    return { reserved: arrayReserved, error: arrayError, definedByUser: arrayDefinedByUser };
  }
  public compareTo(string) {
    let compare = this.table.reservedWorks().filter(value => value === string);
    let isValid = compare.length > 0 ? true : false;
    if (isValid) {
      return { core: compare[0], status: 'Reserved', message: 'Palabra reservada' };
    } else if (!isValid) {
      let detectedStatus = this.isValid(string);
      if (detectedStatus.type === 'Success') {
        return { core: string, status: 'User', message: 'Definido por el usuario' };
      } else if (detectedStatus.type === 'Error'){
        return { core: string, status: 'Error', message: detectedStatus.message };
      }
    }
  }
  private typeToken(token){
    let arrayToken = token.split('');
    if(arrayToken.length === 0){
      return  this.identify(arrayToken[0]).type
    }else{
      let TypeString=0;
      let TypeNumber=0;
      let TypeSymbol=0;
      for(let item of arrayToken){
        let rest =  this.identify(arrayToken[0]).type;
        if(rest === 'Letter'){TypeString++;}
        if(rest === 'Number'){TypeNumber++;}
        if(rest === 'Error'){TypeSymbol++;}
      }
      if(arrayToken.length === TypeString){ return 'String' }
      if(arrayToken.length === TypeNumber){ return 'Number' }
      if(arrayToken.length === TypeSymbol){ return 'Symbol' }
    }
  }
  private repeatToken(origin, key) {
    let Units = [
      'programa',
      'variables',
      'inicio',
      'fin'
    ]; 
    // let result = arigi

  }
  public testing(data) {
    return this.separator.getStringsLines(data);
  }

  public repeat(array, search) {
    return array.filter(value => value === search) > 1 ? true : false;
  }


}
