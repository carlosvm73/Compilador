import { Separatedoflines } from './separatedoflines';
export class Table {
  public reservedWorks() {
    return [
      'funcion',
      'programa',
      'procedimiento',
      'constante',
      'variables',
      'inicio',
      'fin',
      'arreglo',
      'de',
      'entero',
      'real',
      'logico',
      'si',
      'entonces',
      'casoContrario',
      'repetir',
      'hasta',
      'para',
      'hacer',
      'mientras',
      'leer',
      'escribir',
      'entrada',
      'salida',
      // //Definicion de constantes para los operadores
      '=',
      '<',
      '>',
      '+',
      '-',
      '*',
      '/',
      // //Definicion de constantes para otros simbolos
      ';',
      '.',
      ':',
      ',',
      '(',
      ')',
      '{',
      '}',
      '..',
      ':=',
      '<=',
      '>=',
      '<>',
      '"'
    ];
 
  }
}
