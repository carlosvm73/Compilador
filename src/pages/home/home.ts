import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Elastic } from '../directives/elastic';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

}
